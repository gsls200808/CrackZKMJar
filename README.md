# CrackZKMJar

#### 项目介绍
这时一个ZKM 9.0.8的ZKM的Jar包类文件修改替换自动打包工具

修改了原有jar的如下信息
1. 试用时间信息
2. 版权信息说明
3. 部分标题栏文字


#### 软件架构
使用javaassist替换字节码，使用java.util.zip自动解压和压缩，解决手动替换容易出错的烦恼


#### 安装教程

1. 先将base.jar放到E盘根目录，如果没有E盘，请修改代码中的String oldJarPath = "E:\ZKM.jar"为实际路径


#### 使用说明

1. 直接运行:
```
cd /path/to/project
mvn compile 
mvn exec:java -Dexec.mainClass="com.vvvtimes.main.Main" 
```
2. 打包成jar运行:

打包
```
mvn package
```
运行
```
java -jar CrackBaseJar-1.0-SNAPSHOT-jar-with-dependencies.jar
```
3. 生成的文件会放加上_Crack后缀，并且与原文件在同一目录，重命名替换原文件即可

#### 感谢
1. ilanyu提供的试用版jar包


#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request
