package com.vvvtimes.utils;


import java.io.IOException;

import javassist.CannotCompileException;
import javassist.ClassPool;
import javassist.CtClass;
import javassist.CtMethod;
import javassist.NotFoundException;
import javassist.expr.ExprEditor;
import javassist.expr.MethodCall;

public class CrackClass {

    public static void crack(String oldJarPath, String outClassPath) throws NotFoundException, CannotCompileException, IOException {
    
        ClassPool pool = ClassPool.getDefault();
        pool.insertClassPath(oldJarPath);
        

        try {
        	
        	//修改lh的a方法，控制过期时间
        	CtClass ctClass1 = pool.get("com.zelix.lh");
            CtMethod method = ctClass1.getDeclaredMethod("a");
        	method.setBody("{\n" + 
					"		if ($1.equals(\"BZLJ70ifAHJAW\")) {\n" + 
					"			return 9999999999000L;\n" + 
					"		}\n" + 
					"		if ($1.equals(\"MlkOI0eeAe\")) {\n" + 
					"			return 92233720368547680L;\n" + 
					"		}\n" + 
					"		if ($1.equals(\"BlWcKKUU0\")) {\n" + 
					"			return 5764607523034230L;\n" + 
					"		}\n" + 
					"		return 0L;\n" + 
					"	}");
            ctClass1.writeFile(outClassPath);
            
            //修改jj的两个b方法，控制控制台的版权信息显示
            //修改jj的a方法，控制About页和我同意页的过期时间显示
            CtClass ctClass2= pool.get("com.zelix.jj");
            CtMethod[] methods1 = ctClass2.getDeclaredMethods("b");
			for (CtMethod ctMethod : methods1) {
				CtClass[] cpt = ctMethod.getParameterTypes();

				if (cpt.length == 3) {
					String rtName = ctMethod.getReturnType().getSimpleName();
					if (rtName.equals("String")) {
						ctMethod.insertBefore("{if($1 == -1077707640 && $2 == 1077723102 && $3 == 1077734932) {\n"
								+ "			return \" (Cracked by gsls200808)\";\n" + "		}\n" + "	}");
					}
				} else if (cpt.length == 2) {
					String rtName = ctMethod.getReturnType().getSimpleName();
					if (rtName.equals("String")) {
						ctMethod.insertBefore("{\n" + "		if($1 == 1 && $2 == 732523108) {\n"
								+ "			return \"Cracked by gsls200808\";\n"
								+ "		}else if($1 == 3 && $2 == 732523108) {\n"
								+ "			return \"Unlimited\";\n"
								+ "		}else if($1 == 5 && $2 == 732523108) {\n"
								+ "			return \"gsls200808\";\n"
								+ "		}else if($1 == 7 && $2 == 732523108) {\n"
								+ "			return \"980227548@qq.com\";\n" + "		}\n" + "	}");
					}
				}

			}

			CtMethod[] methods2 = ctClass2.getDeclaredMethods("a");
			for (CtMethod ctMethod : methods2) {
				CtClass[] cpt = ctMethod.getParameterTypes();

				if (cpt.length == 5) {
					String rtName = ctMethod.getReturnType().getSimpleName();
					if (rtName.equals("String")) {
						ctMethod.insertAfter("{" + "		return \"\";\n" + "	}");
					}
				}

			}
			ctClass2.writeFile(outClassPath);
			
			CtClass ctClass3= pool.get("com.zelix.qs");
            CtMethod[] methods3 = ctClass3.getDeclaredMethods("a");
            for (CtMethod ctMethod : methods3) {
				CtClass[] cpt = ctMethod.getParameterTypes();
				if (cpt.length == 5) {
					ctMethod.insertBefore("{"
							+ "		String[] g6 = new String[] { \"License No.:\", \"Cracked by gsls200808\", \"License Type:\", \"Unlimited\", \"Licensee:\", \"gsls200808\", \"\", \"980227548@qq.com\" };\n"
							+ "		for (int i = 0; i < this.g.length; i++) {\n"
							+ "			this.g[i] = g6[i];\n" + "		}" + "}");
				}

			}
            ctClass3.writeFile(outClassPath);

			System.out.println("反编译class修改成功");
        } catch (NotFoundException e) {
			System.out.println("反编译class异常:" + e);
        }
    }

}
